import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import * as Yup from "yup";
import axios from "axios";

import { AppForm, AppFormField, SubmitButton } from "../components/forms";
import LoginHeader from "../components/LoginHeader";
import AppText from "../components/AppText";

import Colors from "../constants/colors";

const validationSchema = Yup.object().shape({
  email: Yup.string().required().email().label("Email"),
});

const ResetScreen = ({ navigation }) => {
  const handleSubmit = async (values) => {
    try {
      const response = await axios.post("/users/USER/reset_tokens", values);
      console.log(response);
      await navigation.navigate("Token");
    } catch (error) {
      const data = error.response.data;
      console.log(data);
    }
  };

  return (
    <View style={styles.boxWrapper}>
      <LoginHeader login="Reset Password" subText="Please enter Your Email" />
      <View style={styles.middle}>
        <AppForm
          initialValues={{ email: "" }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <View style={styles.middle}>
            <AppFormField
              placeholder="E-mail"
              icon="email-outline"
              name="email"
            />

            <SubmitButton title="Send" />
          </View>
        </AppForm>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  boxWrapper: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "100%",
  },
  middle: { padding: 10 },
});

export default ResetScreen;
