import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import * as Yup from "yup";
import axios from "axios";

import { AppForm, AppFormField, SubmitButton } from "../components/forms";
import LoginHeader from "../components/LoginHeader";
import AppText from "../components/AppText";

import Colors from "../constants/colors";

const validationSchema = Yup.object().shape({
  token: Yup.string().required().min(6).max(6).label("Token"),
  password: Yup.string().required("Password is required"),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Passwords must match"
  ),
});

const TokenScreen = ({ navigation }) => {
  const handleSubmit = async (values) => {
    try {
      const response = await axios.put("users/USER/reset_tokens", values);
      console.log(response);
      await navigation.navigate("Login");
    } catch (error) {
      const data = error.response.data;
      console.log(data);
    }
  };
  return (
    <View style={styles.boxWrapper}>
      <LoginHeader
        login="Reset Password"
        subText="Please Enter You Token from Email and New Password"
      />
      <View style={styles.middle}>
        <AppForm
          initialValues={{ token: "" }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <View style={styles.middle}>
            <AppFormField
              placeholder="Token"
              name="token"
              autoCapitalize="none"
            />
            <AppFormField
              placeholder="Password"
              icon="lock-outline"
              secureTextEntry={true}
              name="password"
            />

            <AppFormField
              placeholder="Repeat Password"
              icon="lock-outline"
              secureTextEntry={true}
              name="passwordConfirmation"
            />

            <SubmitButton title="continue" />
          </View>
        </AppForm>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  boxWrapper: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "100%",
  },
  middle: { padding: 10 },
});

export default TokenScreen;
