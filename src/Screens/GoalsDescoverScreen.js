import React from "react";
import { View, Text, StyleSheet } from "react-native";

import Screen from "../components/Screen";
import ItemView from "../components/ItemView";

import Colors from "../constants/colors";

const GoalsDescoverScreen = () => {
  return (
    <Screen style={styles.container}>
      <ItemView />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
  },
});

export default GoalsDescoverScreen;
