import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";

import Screen from "../components/Screen";
import AppButton from "../components/AppButton";
import Colors from "../constants/colors";

const WelcomeScreen = ({ navigation }) => {
  return (
    <Screen style={styles.container}>
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require("../../assets/logo.jpg")} />
        <Text style={styles.text}>Fundraising</Text>
      </View>
      <View style={styles.buttonsConainer}>
        <AppButton
          title="Login"
          textColor="mainColor"
          onPress={() => navigation.navigate("Login")}
        />
        <AppButton
          title="Register"
          color="mainColor"
          onPress={() => navigation.navigate("Register")}
        />
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  logoContainer: {
    position: "absolute",
    top: 80,
    alignItems: "center",
  },
  logo: {
    width: 150,
    height: 150,
    borderWidth: 1,
    borderRadius: 100,
  },
  text: {
    color: "white",
    fontSize: 25,
    fontWeight: "600",
    paddingVertical: 20,
  },
  buttonsConainer: {
    padding: 23,
    width: "100%",
  },
});

export default WelcomeScreen;
