import React from "react";
import { View, Text, StyleSheet } from "react-native";
import * as Yup from "yup";
import axios from "axios";

import { AppForm, AppFormField, SubmitButton } from "../components/forms";
import Screen from "../components/Screen";

import Colors from "../constants/colors";

const validationSchema = Yup.object().shape({
  userName: Yup.string().required().label("Name"),
  birthday: Yup.date().required().label("Birthday"),
  country: Yup.string().required().label("Country"),
});

const UserInfoEditScreen = ({ navigation }) => {
  const handleSubmit = async (values) => {
    const { ...restProps } = values;
    console.log(values);
    const payload = {
      role: "USER",
      data: {
        ...restProps,
      },
    };
    try {
      const response = await axios.put("users/USER", payload);
      console.log(response);
      await navigation.push("Profile");
    } catch (error) {
      const data = error.response.data;
      console.log(data);
    }
  };
  return (
    <Screen style={styles.container}>
      <AppForm
        initialValues={{
          userName: "",
          birthday: "",
          country: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <View style={styles.middle}>
          <AppFormField
            placeholder="Full name"
            icon="account-outline"
            name="userName"
          />
          <AppFormField
            placeholder="Birthday"
            icon="calendar-month-outline"
            name="birthday"
          />
          <AppFormField
            placeholder="Country"
            icon="city-variant-outline"
            name="country"
          />

          <SubmitButton title="Save" />
        </View>
      </AppForm>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
  },
  middle: { padding: 20 },
});

export default UserInfoEditScreen;
