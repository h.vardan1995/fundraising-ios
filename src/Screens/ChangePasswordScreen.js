import React, { useContext } from "react";
import { View, Text, StyleSheet } from "react-native";
import * as Yup from "yup";
import axios from "axios";

import {
  AppForm,
  AppFormField,
  SubmitButton,
  ErrorMessage,
} from "../components/forms";
import Screen from "../components/Screen";
import LoginHeader from "../components/LoginHeader";

import AuthContext from "../auth/context";

import Colors from "../constants/colors";

const validationSchema = Yup.object().shape({
  oldPassword: Yup.string().required("Password is required"),
  password: Yup.string().required("Password is required"),
  repeatNewPassword: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Passwords must match"
  ),
});

const ChangePasswordScreen = () => {
  const { user } = useContext(AuthContext);
  const userId = user.userId;

  const handleSubmit = async (values) => {
    const { password, oldPassword } = values;
    const payload = {
      role: "USER",
      userId,
      password,
      oldPassword,
    };
    try {
      const response = await axios.put(
        `/users/USER/${userId}/password`,
        payload
      );
      console.log(response);
      navigation.push("Profile");
    } catch (error) {
      const data = error.response.data;
      console.log(data);
    }
  };

  return (
    <Screen style={styles.container}>
      <LoginHeader
        login="Reset Password"
        subText="Please Enter Your New Password"
      />

      <AppForm
        initialValues={{
          email: "", //vardanh@redkite.io
          password: "", //User123!
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <View style={styles.middle}>
          <AppFormField
            placeholder="Old Password"
            icon="lock-outline"
            secureTextEntry={true}
            name="oldPassword"
          />
          <AppFormField
            placeholder="New Password"
            icon="lock-outline"
            secureTextEntry={true}
            name="password"
          />

          <AppFormField
            placeholder="Repeat New Password"
            icon="lock-outline"
            secureTextEntry={true}
            name="repeatNewPassword"
          />

          <SubmitButton title="Confirm" />
        </View>
      </AppForm>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "flex-start",
  },
  middle: { padding: 20 },
});

export default ChangePasswordScreen;
