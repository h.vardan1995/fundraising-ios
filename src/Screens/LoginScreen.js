import React, { useState, useContext } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import * as Yup from "yup";
import axios from "axios";

import Screen from "../components/Screen";
import {
  AppForm,
  AppFormField,
  SubmitButton,
  ErrorMessage,
} from "../components/forms";
import LoginHeader from "../components/LoginHeader";
import AppText from "../components/AppText";
import AuthContext from "../auth/context";

import Colors from "../constants/colors";

const validationSchema = Yup.object().shape({
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().label("Password"),
});

const LoginScreen = ({ navigation }) => {
  const { setUser } = useContext(AuthContext);
  const [loginFailed, setLoginFailed] = useState(false);

  const handleSubmit = async (loginInfo) => {
    try {
      const response = await axios.post(
        "https://fndrisr.herokuapp.com/login",
        loginInfo
      );
      setUser(response.data);
    } catch (error) {
      const data = error.response.data;
      if (!data) return setLoginFailed(false);
      setLoginFailed(true);
    }
  };

  return (
    <Screen style={styles.boxWrapper}>
      <LoginHeader login="Login" subText="Please sign in to continue" />

      <AppForm
        initialValues={{
          email: "h.vardan1995@gmail.com", //vardanh@redkite.io ___ h.vardan1995@gmail.com ___ eddy.hovhannisyan@gmail.com
          password: "User123!", //User12345! ___ User123! ___ Test1234#
        }}
        // initialValues={{
        //   email: "eddy.hovhannisyan@gmail.com", //vardanh@redkite.io ___ h.vardan1995@gmail.com ___ eddy.hovhannisyan@gmail.com
        //   password: "Test1234#", //User12345! ___ User123! ___ Test1234#
        // }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <View style={styles.middle}>
          <ErrorMessage
            error="Invalid email and/or password"
            visible={loginFailed}
          />
          <AppFormField
            placeholder="E-mail"
            icon="email-outline"
            name="email"
            autoCapitalize="none"
          />

          <AppFormField
            placeholder="Password"
            icon="lock-outline"
            secureTextEntry={true}
            name="password"
          />

          <SubmitButton title="Login" />
          <TouchableOpacity
            style={styles.forogtTextBox}
            onPress={() => navigation.navigate("Reset")}
          >
            <AppText style={styles.forogtText}>Forgot Password?</AppText>
          </TouchableOpacity>
        </View>
      </AppForm>
    </Screen>
  );
};

const styles = StyleSheet.create({
  boxWrapper: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "flex-start",
  },
  start: { padding: 20 },
  middle: { padding: 20 },
  end: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "center",
    paddingBottom: 40,
  },
  loginText: {
    color: Colors.white,
    fontSize: 45,
    fontWeight: "bold",
  },
  signInText: {
    fontSize: 14,
    color: Colors.inputText,
  },
  bottomText: {
    color: Colors.inputText,
  },
  signInText1: {
    color: Colors.mainGreen,
  },
  loginScreenButton: {
    marginTop: 60,
    marginHorizontal: "10%",
    paddingVertical: 10,
    backgroundColor: Colors.mainGreen,
    borderRadius: 25,
    fontSize: 30,
  },
  forogtTextBox: {
    flexDirection: "column",
    marginTop: "70%",
    alignItems: "center",
  },
  forogtText: {
    color: Colors.mainGreen,
  },
});

export default LoginScreen;
