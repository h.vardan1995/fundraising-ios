import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import * as Yup from "yup";
import axios from "axios";

import { AppForm, AppFormField, SubmitButton } from "../components/forms";
import LoginHeader from "../components/LoginHeader";
import Screen from "../components/Screen";
import AppText from "../components/AppText";
import Colors from "../constants/colors";

const validationSchema = Yup.object().shape({
  userName: Yup.string().required().label("Name"),
  birthday: Yup.date().required().label("Birthday"),
  country: Yup.string().required().label("Country"),
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required("Password is required"),
  passwordConfirmation: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Passwords must match"
  ),
});

const RegisterScreen = () => {
  const handleSubmit = async (values) => {
    const { email, password, userName, ...restProps } = values;

    console.log(values);
    const payload = {
      email,
      role: "USER",
      password,
      data: {
        userName,
        ...restProps,
      },
    };
    try {
      const response = await axios.post("/users/USER", payload);
      console.log(response);
    } catch (error) {
      const data = error.response.data;
      console.log(data);
    }
  };

  return (
    <Screen style={styles.container}>
      <LoginHeader
        login="Create Account"
        subText=" Please fill the input below here"
      />

      <AppForm
        initialValues={{
          userName: "",
          birthday: "",
          country: "",
          email: "",
          password: "",
          passwordConfirmation: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <View style={styles.middle}>
          <AppFormField
            placeholder="Full name"
            icon="account-outline"
            name="userName"
          />
          <AppFormField
            placeholder="Birthday"
            icon="calendar-month-outline"
            name="birthday"
          />
          <AppFormField
            placeholder="Country"
            icon="city-variant-outline"
            name="country"
          />
          <AppFormField placeholder="Email" icon="email-outline" name="email" />

          <AppFormField
            placeholder="Password"
            icon="lock-outline"
            secureTextEntry={true}
            name="password"
          />

          <AppFormField
            placeholder="Password"
            icon="lock-outline"
            secureTextEntry={true}
            name="passwordConfirmation"
          />

          <SubmitButton title="Register" />
        </View>
      </AppForm>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "flex-start",
  },
  start: { padding: 20 },
  middle: { padding: 20 },
  end: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "center",
    paddingBottom: 40,
  },
  loginText: {
    color: Colors.white,
    fontSize: 45,
    fontWeight: "bold",
  },
  signInText: {
    fontSize: 14,
    color: Colors.inputText,
  },
  bottomText: {
    color: Colors.inputText,
  },
  signInText1: {
    color: Colors.mainGreen,
    marginBottom: -2,
    marginLeft: 5,
  },
  loginScreenButton: {
    marginTop: 20,
    marginHorizontal: "10%",
    paddingVertical: 10,
    backgroundColor: Colors.mainGreen,
    borderRadius: 25,
    fontSize: 30,
  },
});

export default RegisterScreen;
