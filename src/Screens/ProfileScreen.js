import React, { useContext, useState, useEffect, useMemo } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
import axios from "axios";

import Screen from "../components/Screen";
import AppButton from "../components/AppButton";
import AppText from "../components/AppText";
import ProfileInfo from "../components/ProfileInfo";

import AuthContext from "../auth/context";

import Colors from "../constants/colors";

const ProfileScreen = ({ navigation }) => {
  const [apiData, setApiData] = useState("");
  const { setUser, user } = useContext(AuthContext);

  const handleLogOut = () => {
    setUser(null);
  };

  useEffect(() => {
    getAllData();
    console.log("data");
  }, []);

  const getAllData = async () => {
    try {
      const response = await axios.get("/users/USER");
      setTimeout(() => {
        setApiData(response.data);
      }, 2000);
    } catch (error) {
      const data = error.response.data;
      console.log(data);
    }
  };

  console.log("apiData", apiData);

  return (
    <Screen style={styles.container}>
      <View style={styles.userEditBox}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("UserInfoEdit");
          }}
        >
          <MaterialCommunityIcons
            name="account-edit-outline"
            size={30}
            color={Colors.inputText}
            style={styles.icon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Settings")}>
          <Ionicons
            name="ios-settings"
            size={30}
            color={Colors.inputText}
            style={styles.icon}
          />
        </TouchableOpacity>
      </View>

      {!!apiData ? (
        <View>
          <ProfileInfo
            icon="account-outline"
            smallText="Full Name"
            largeText={apiData.data?.userName}
          />

          <ProfileInfo
            icon="calendar-month-outline"
            smallText="Birthday"
            largeText={apiData.data?.birthday}
          />

          <ProfileInfo
            icon="city-variant-outline"
            smallText="Country"
            largeText={apiData.data?.country}
          />

          <ProfileInfo
            icon="email"
            smallText="E-mail"
            largeText={apiData?.email}
          />

          <View style={styles.changePassword}>
            <AppButton
              title="Change Password"
              color={Colors.mainColor}
              onPress={() => navigation.navigate("ChangePasswordScreen")}
            />
          </View>
        </View>
      ) : (
        <ActivityIndicator />
      )}

      <View style={styles.containerBox}>
        <AppButton
          title="Logout"
          textColor="mainColor"
          onPress={handleLogOut}
        />
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  userEditBox: {
    width: "100%",
    backgroundColor: Colors.mainColor2,
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },

  iconsBox: {
    flexDirection: "row",
  },
  changePassword: { margin: 20 },
  containerBox: {
    marginHorizontal: "25%",
  },
});

export default ProfileScreen;
