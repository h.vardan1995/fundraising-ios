import React from "react";
import { View, Text, StyleSheet } from "react-native";

import Screen from "../components/Screen";
import NotificationView from "../components/NotificationView";

import Colors from "../constants/colors";

const NotificationScreen = () => {
  return (
    <Screen style={styles.container}>
      <NotificationView
        title="Reminder-Company Name"
        subTitle="Lorem Ipsum is simply dummy text of the printing and typesetting
          industry."
        img={{ uri: "https://picsum.photos/200/300" }}
      />
      <NotificationView
        title="Notify-Company Name"
        subTitle="Charged: 50$"
        type=""
        img={{ uri: "https://picsum.photos/200/300" }}
      />
      <NotificationView
        title="Reminder-Company Name"
        subTitle="Lorem Ipsum is simply dummy text of the printing and typesetting
          industry."
        img={{ uri: "https://picsum.photos/200/300" }}
      />
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    flexDirection: "column",
    justifyContent: "flex-start",
  },
});

export default NotificationScreen;
