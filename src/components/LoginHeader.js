import React from "react";
import { View, Text, StyleSheet } from "react-native";

import AppText from "../components/AppText";
import Colors from "../constants/colors";

const LoginHeader = ({ login, subText }) => {
  return (
    <View style={styles.start}>
      <AppText style={styles.loginText}>{login}</AppText>
      <AppText style={styles.signInText}>{subText}</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  start: {
    padding: 20,
  },
  loginText: {
    color: Colors.white,
    fontSize: 45,
    fontWeight: "bold",
  },
  signInText: {
    fontSize: 14,
    color: Colors.inputText,
  },
});

export default LoginHeader;
