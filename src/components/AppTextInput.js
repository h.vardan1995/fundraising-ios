import React from "react";
import { View, StyleSheet, TextInput, Platform } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import Colors from "../constants/colors";

const AppTextInput = ({ icon, ...otherProps }) => {
  return (
    <View style={styles.container}>
      {icon && (
        <MaterialCommunityIcons
          name={icon}
          size={20}
          color={Colors.inputText}
          style={styles.icon}
        />
      )}
      <TextInput
        style={styles.textInput}
        {...otherProps}
        placeholderTextColor={Colors.inputText}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor2,
    color: Colors.white,
    borderRadius: 5,
    flexDirection: "row",
    width: "100%",
    padding: 10,
    marginVertical: 5,
  },
  textInput: {
    fontSize: 16,
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
    color: Colors.white,
  },
  icon: {
    marginRight: 10,
  },
});

export default AppTextInput;
