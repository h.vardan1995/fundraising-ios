import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import Colors from "../constants/colors";

const ProfileInfo = ({ icon, smallText, largeText }) => {
  return (
    largeText && (
      <View style={styles.container}>
        <View style={styles.info}>
          <View>
            <MaterialCommunityIcons
              name={icon}
              size={20}
              color={Colors.inputText}
            />
          </View>
          <View style={styles.textInfo}>
            <Text style={styles.smallText}>{smallText}</Text>
            <Text style={styles.largeText}>{largeText}</Text>
          </View>
        </View>
      </View>
    )
  );
};

const styles = StyleSheet.create({
  info: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginHorizontal: 20,
    marginVertical: 5,
    padding: 8,
    borderRadius: 5,
    backgroundColor: Colors.mainColor2,
  },
  textInfo: {
    paddingLeft: 20,
  },
  smallText: {
    fontSize: 12,
    color: Colors.inputText,
  },
  largeText: {
    fontSize: 16,
    fontWeight: "bold",
    color: Colors.inputText,
  },
});

export default ProfileInfo;
