import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import Colors from "../constants/colors";

const DeleteAction = ({ setVisible }) => {
  const deleteHandler = () => {
    setVisible(false);
  };
  return (
    <TouchableOpacity style={styles.container} onPress={deleteHandler}>
      <MaterialCommunityIcons
        name="delete"
        size={25}
        color={Colors.inputText}
        style={styles.icon}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor,
    width: 70,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default DeleteAction;
