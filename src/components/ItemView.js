import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import * as Progress from "react-native-progress";

import AppText from "../components/AppText";

import Colors from "../constants/colors";

const ItemView = () => {
  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.image}
          source={require("../../assets/dummyPic.jpg")}
        />
        <View style={styles.textsWrapper}>
          <AppText style={styles.companyName}>Company Name</AppText>
          <View>
            <AppText style={styles.description}>Description</AppText>
            <AppText numberOfLines={3} style={styles.descriptionText}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s
            </AppText>
          </View>
          <View style={styles.progressBar}>
            <View style={styles.progressBarNumbers}>
              <View>
                <AppText style={styles.number}>7800$</AppText>
              </View>
              <View>
                <AppText style={styles.number}>100000$</AppText>
              </View>
            </View>
            <Progress.Bar
              progress={0.078}
              width={300}
              borderColor={"#3DA99C"}
              color="rgba(61, 169, 156, 1)"
            />
          </View>

          <View style={styles.doateAndShareBox}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                console.log("donated!!!");
              }}
            >
              <Text style={styles.text}>Donate Now</Text>
            </TouchableOpacity>
            <MaterialCommunityIcons
              name="share-variant"
              size={25}
              color={Colors.inputText}
              style={styles.icon}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mainColor2,
    width: "94%",
    height: "72%",
    borderRadius: 15,

    marginLeft: "3%",
  },
  image: {
    width: "100%",
    height: "50%",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  textsWrapper: {
    padding: 10,
  },
  companyName: {
    fontSize: 18,
    color: "white",
    paddingBottom: 10,
  },
  description: {
    fontSize: 14,
    color: Colors.inputText,
    paddingBottom: 2,
  },
  descriptionText: {
    fontSize: 14,
    color: Colors.inputText,
    paddingBottom: 2,
  },
  progressBar: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: "5%",
  },
  progressBarNumbers: {
    width: "100%",
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 40,
  },
  number: { color: "white" },
  doateAndShareBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 20,
  },
  button: {
    backgroundColor: Colors.mainGreen,
    borderRadius: 5,

    borderColor: Colors.mainGreen,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    width: "40%",
  },
  text: {
    color: Colors.mainColor,
    fontSize: 13,
    textTransform: "uppercase",
    fontWeight: "bold",
  },
});

export default ItemView;
