import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";

import Colors from "../constants/colors";
const AppButton = ({
  title,
  onPress,
  color = "mainGreen",
  textColor = "mainGreen",
  style,
  styleText,
}) => {
  return (
    <TouchableOpacity
      style={[styles.button, { backgroundColor: Colors[color] }, style]}
      onPress={onPress}
    >
      <Text style={[styles.text, { color: Colors[textColor] }, styleText]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.mainGreen,
    borderRadius: 28,
    borderWidth: 3,
    borderColor: Colors.mainGreen,
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    width: "100%",
    marginVertical: 10,
  },
  text: {
    color: Colors.mainColor,
    fontSize: 18,
    textTransform: "uppercase",
    fontWeight: "bold",
  },
});

export default AppButton;
