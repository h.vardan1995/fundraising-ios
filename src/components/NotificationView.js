import React, { useState } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import Swipeable from "react-native-gesture-handler/Swipeable";

import Colors from "../constants/colors";
import AppButton from "../components/AppButton";
import DeleteAction from "../components/DeleteAction";

const NotificationView = ({ title, subTitle, type = "withDonate", img }) => {
  const [visible, setVisible] = useState(true);
  return (
    !!visible && (
      <Swipeable
        renderRightActions={() => <DeleteAction setVisible={setVisible} />}
      >
        <View style={styles.containerWrapper}>
          <View style={styles.container}>
            <View style={styles.logoContainer}>
              <Image style={styles.logo} source={img} />
            </View>
            <View style={styles.textWrapper}>
              <Text style={styles.textTitle}>{title}</Text>
              <Text style={styles.textSubtitle} numberOfLines={2}>
                {subTitle}
              </Text>
            </View>
          </View>
          {type === "withDonate" ? (
            <View style={styles.buttonWrapper}>
              <AppButton
                title="Donate Now"
                textColor="mainColor"
                style={styles.button}
                styleText={styles.buttonText}
              />
            </View>
          ) : null}
        </View>
      </Swipeable>
    )
  );
};

const styles = StyleSheet.create({
  containerWrapper: {
    width: "94%",
    backgroundColor: Colors.mainColor2,
    paddingHorizontal: "5%",
    paddingVertical: 15,
    marginHorizontal: "3%",
    marginVertical: 5,
    borderRadius: 5,
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
  },

  logo: {
    width: 58,
    height: 58,
    borderRadius: 100,
  },
  buttonWrapper: {
    alignItems: "flex-end",
    marginTop: 10,
  },
  button: {
    borderRadius: 8,
    width: "50%",
    padding: 7,
    borderWidth: 0,
    flexDirection: "row",
  },
  buttonText: {
    textTransform: "capitalize",
    fontSize: 15,
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: 10,
  },
  textTitle: {
    color: "white",
    fontSize: 17,
    paddingBottom: 3,
  },
  textSubtitle: {
    color: Colors.inputText,
    fontSize: 13,
  },
});

export default NotificationView;
