export default {
  mainColor: "#201A30",
  mainColor2: "#38304C",
  inputText: "#9C9C9C",
  mainGreen: "#3DA99C",
  white: "#ffffff",
  headerBox: "#37314E",
  boxBackground: "#3A354A",
};
