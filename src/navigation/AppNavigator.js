import * as React from "react";

import GoalsDescoverScreen from "../Screens/GoalsDescoverScreen";
import NotificationScreen from "../Screens/NotificationScreen";
import ProfileScreen from "../Screens/ProfileScreen";

import ChangePasswordNavigator from "./ChangePasswordNavigator";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import Colors from "../constants/colors";

const Tab = createBottomTabNavigator();

const AppNavigator = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        labelStyle: {
          fontSize: 13,
        },
        tabStyle: {
          fontSize: 10,
          margin: 5,
        },
        style: {
          backgroundColor: Colors.mainColor2,
        },
      }}
    >
      <Tab.Screen
        name="GoalsDescover"
        component={GoalsDescoverScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="home-outline"
              size={size}
              color={color}
            />
          ),
          tabBarLabel: "Home",
        }}
      />
      <Tab.Screen
        name="Notification"
        component={NotificationScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="bell-outline"
              size={size}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ChangePasswordNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="account-outline"
              size={size}
              color={color}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default AppNavigator;
