import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import WelcomeScreen from "../Screens/WelcomeScreen";
import LoginScreen from "../Screens/LoginScreen";
import RegisterScreen from "../Screens/RegisterScreen";

import ResetScreen from "../Screens/ResetScreen";
import TokenScreen from "../Screens/TokenScreen";

import ProfileScreen from "../Screens/ProfileScreen";

import Colors from "../constants/colors";

const Stack = createStackNavigator();

const AuthNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Welcome"
      component={WelcomeScreen}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Login"
      component={LoginScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
      }}
    />
    <Stack.Screen
      name="Register"
      component={RegisterScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
      }}
    />
    <Stack.Screen
      name="Reset"
      component={ResetScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
      }}
    ></Stack.Screen>

    <Stack.Screen
      name="Token"
      component={TokenScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
      }}
    ></Stack.Screen>
  </Stack.Navigator>
);

export default AuthNavigator;
