import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import ProfileScreen from "../Screens/ProfileScreen";
import ChangePasswordScreen from "../Screens/ChangePasswordScreen";
import UserInfoEditScreen from "../Screens/UserInfoEditScreen";
import SettingsScreen from "../Screens/SettingsScreen";

import Colors from "../constants/colors";

const Stack = createStackNavigator();

export default () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Profile"
      component={ProfileScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="ChangePasswordScreen"
      component={ChangePasswordScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
        title: "Change Password",
      }}
    />
    <Stack.Screen
      name="UserInfoEdit"
      component={UserInfoEditScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
        title: "Edit",
      }}
    />
    <Stack.Screen
      name="Settings"
      component={SettingsScreen}
      options={{
        headerStyle: { backgroundColor: Colors.mainColor },
        headerTintColor: "white",
        title: "settings",
      }}
    />
  </Stack.Navigator>
);
