import axios from "axios";

axios.defaults.baseURL = `https://fndrisr.herokuapp.com`;

export default axios;
