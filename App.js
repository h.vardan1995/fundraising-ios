import React from "react";
import axios from "./src/configs/axios";

import { NavigationContainer } from "@react-navigation/native";

import AuthNavigator from "./src/navigation/AuthNavigator";
import AppNavigator from "./src/navigation/AppNavigator";
import navigationTheme from "./src/navigation/navigationTheme";
import AuthContext from "./src/auth/context";
import useLocalStorage from "./src/hooks/useLocalStorage.effect";
import { useEffect } from "react/cjs/react.development";

let token;
axios.interceptors.request.use((request) => {
  if (token) request.headers.Authorization = `Bearer ${token}`;
  return request;
});

export default function App() {
  const [user, setUser] = useLocalStorage("userData", null);
  const userToken = user?.token;

  useEffect(() => {
    token = userToken;
  }, [userToken]);

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      <NavigationContainer theme={navigationTheme}>
        {user?.token ? <AppNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
